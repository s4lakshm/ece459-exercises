use std::thread;
use std::time::Duration;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];
    for i in 0..N{
        let child = thread::spawn(move || {
            thread::sleep(Duration::from_millis(500));
            println!("Thread {} done sleeping!", i);
        });
        children.push(child);
    }
    for child in children{
        child.join().unwrap()
    }
}
