use std::sync::mpsc;
use std::thread;
use std::time::Duration;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();
    let N = 10;

    for i in 0..N{
        let tx_clone = mpsc::Sender::clone(&tx);
        thread::spawn(move || {
            let val = String::from(format!("Me from {}", i));
            thread::sleep(Duration::from_millis(10));
            tx_clone.send(val).unwrap();
            drop(tx_clone);
        });

    }
    drop(tx);
    // All senders closed = channel closes

    for received in rx {
        println!("Got: {}", received);
    }
}
