// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    if n <= 1 { return n}
    else if n < 0{ return 0 }
    return fibonacci_number(n-2) + fibonacci_number(n-1)
}


fn main() {
    println!("{}", fibonacci_number(10));
}
